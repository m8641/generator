#!/bin/bash

##########################################
##
##  Generateur de docker-compose
##
##########################################


##Variables #############################

DIR="${HOME}/generator"
USER_SCRIPT=${USER}

## Functions ###########################

help(){

echo "USAGE :
   ${0##*/} [-h] [--help]
Options :

 -h, --help : aides
 -p, --postgres : lance une instance postgres
 -m, --mysql: lance une instance mysql
 -i, --ip : affichage des ip

"
}

ip(){

      for i in $(docker ps -q);do docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} - {{.Name}}" $i  ;done
}

parser_options(){

case $@ in
        -h|--help)
          help
        ;;

        -p|--postgres)
         postgres
        ;;

       -m|--mysql)
         mysql
        ;;


       *)
       echo  "options invalide, lancez -h ou --help"

esac
}

mysql(){

echo ""
echo "Installation d'une instance mysql...."
echo ""
echo "1 - Creation du repertoire de datas .."

 mkdir -p $DIR

echo "
version: '3.7'
services:
   etatcivil-mysql-db:
     privileged: true
     user: root
     image: mysql:latest
     command: --default-authentication-plugin=mysql_native_password

     ports:
       - "3306:3306"
     networks:
       - generator
     container_name: mysql_serveur
     environment:
       - MYSQL_ROOT_PASSWORD=kamisama123
       - MYSQL_USER=wachehi
       - MYSQL_PASSWORD=Madjikha6901
       - MYSQL_DATABASE=BaseEtatCivilComores
volumes:
   mysql_data:
     driver: local
     driver_opts:
       o: bind
       type: none
       device: /srv/mysql
networks:
  generator:
    driver: bridge
" >$DIR/docker-compose-mysql.yml

echo "2 - Run de l'instance ..."
sudo docker-compose -f $DIR/docker-compose-mysql.yml  up  -d

}

postgres() {

echo ""
echo "Installation d'une instance postgres...."
echo ""
echo "1 - Creation du repertoire de datas .."
sudo  mkdir -p $DIR
echo "
version: '3.7'
services:
  postgres:
    privileged: true
    user: root
    image: postgres:latest
    container_name: postgres
    environment:
    - POSTGRES_USER=wachehi
    - POSTGRES_PASSWORD=Madjikha6901
    - POSTGRES_DATABASE=BaseEtatCivilComores
    ports:
    -  5432:5432
    volumes:
    - postgres_data:/var/lib/postgres
    networks:
    - generator
volumes:
   postgres_data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: /srv/postgres
networks:
  generator:
    driver: bridge

" > $DIR/docker-compose-postgres.yml

echo "2 - Run de l'instance ..."
sudo docker-compose -f $DIR/docker-compose-postgres.yml  up  -d

echo ""
echo "
Credentials :

    - PORT : 5432
    - POSTGRES_USER: wachehi
    - POSTGRES_PASSWORD: Madjikha6901
    - POSTGRES_DATAGASE: BaseEtatCivilComores

Command : psql -h <ip> -u myuser -d  mydb

"
}

## Execute #############################


parser_options  $@
ip

